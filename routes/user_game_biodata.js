var express = require("express");
var router = express.Router();

const models = require("../models");
const userBiodataModel = models.user_game_biodata;

// menampilkan semua biodata user
router.get("/", async (req, res, next) => {
  const getAllUserBiodata = await userBiodataModel.findAll();
  res.json(getAllUserBiodata);
});

// menampilkan data user_game_biodata by id
router.get("/:id", async (req, res, next) => {
  const userGameBiodataId = parseInt(req.params.id);
  const getUserGameBiodataById = await userBiodataModel.findByPk(
    userGameBiodataId
  );

  res.json({
    data: getUserGameBiodataById,
  });
});

// update data user_game_biodata
router.put("/:id", async (req, res, next) => {
  const userGameBiodataId = parseInt(req.params.id);
  const fullname = req.body.fullname;
  const age = req.body.age;
  const address = req.body.address;

  try {
    const updatedUserGameBiodata = await userBiodataModel.update(
      {
        fullname,
        age,
        address,
      },
      {
        where: {
          id: userGameBiodataId,
        },
      }
    );
    const getUserGameBiodataById = await userBiodataModel.findByPk(
      userGameBiodataId
    );
    res.json({
      msg: `Success update data with id ${userGameBiodataId}`,
      dataUpdated: getUserGameBiodataById,
    });
  } catch (error) {
    res.status(500).json({
      error: error,
    });
  }
});

module.exports = router;
