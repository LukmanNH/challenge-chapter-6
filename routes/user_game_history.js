const express = require("express");
const router = express.Router();

const models = require("../models");
const userGameHistoryModel = models.user_game_history;

// menampilkan semua data history
router.get("/", async (req, res, next) => {
  const getAllUserGameHistoryData = await userGameHistoryModel.findAll();
  res.json(getAllUserGameHistoryData);
});

// menampilkan data user_game_history by id
router.get("/:id", async (req, res, next) => {
  const userGameHistoryId = parseInt(req.params.id);
  const getUserGameHistoryById = await userGameHistoryModel.findByPk(
    userGameHistoryId
  );

  res.json({
    data: getUserGameHistoryById,
  });
});

// update data user_game_history
router.put("/:id", async (req, res, next) => {
  const userGameHistoryId = parseInt(req.params.id);
  const hoursOfPlayedGame = req.body.hoursOfPlayedGame;
  const mostPlayedGame = req.body.mostPlayedGame;

  try {
    await userGameHistoryModel.update(
      {
        hoursOfPlayedGame,
        mostPlayedGame,
      },
      {
        where: {
          id: userGameHistoryId,
        },
      }
    );

    const getUpdatedData = await userGameHistoryModel.findByPk(
      userGameHistoryId
    );
    res.json({
      msg: `Success update data with id ${userGameHistoryId}`,
      data: getUpdatedData,
    });
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

module.exports = router;
