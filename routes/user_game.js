var express = require("express");
var router = express.Router();

const models = require("../models");
const userGameModel = models.user_game;
const userGameBiodataModel = models.user_game_biodata;
const userGameHistoryModel = models.user_game_history;

// Menampilkan semua data including biodata dan history
router.get("/", async function (req, res, next) {
  try {
    const userGameData = await userGameModel.findAll({
      include: ["biodata", "history"],
    });
    res.json(userGameData);
  } catch (error) {
    res.status(500).json({
      error: error,
    });
  }
});

// menampilkan spesifik data dengan id (semua attribute)
router.get("/:id", async (req, res, next) => {
  try {
    const userGameId = parseInt(req.params.id);
    const userGameData = await userGameModel.findAll({
      include: ["biodata", "history"],
      where: {
        id: userGameId,
      },
    });
    res.json(userGameData);
  } catch (error) {
    res.status(500).json({ error: error });
  }
});

// input 3 table (user, biodata, history) dengan validasi username tidak boleh sama
router.post("/", async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const email = req.body.email;
  const fullname = req.body.fullname;
  const age = req.body.age;
  const hoursOfPlayedGame = req.body.hoursOfPlayedGame;
  const mostPlayedGame = req.body.mostPlayedGame;
  const address = req.body.address;

  try {
    const findUsername = await userGameModel.findOne({
      where: {
        username: username,
      },
    });
    if (!findUsername) {
      const userGameSave = await userGameModel.create({
        username,
        password,
        email,
      });
      const userGameId = userGameSave.id;
      await userGameBiodataModel.create({
        fullname,
        age,
        address,
        user_game_id: userGameId,
      });

      await userGameHistoryModel.create({
        hoursOfPlayedGame,
        mostPlayedGame,
        user_game_id: userGameId,
      });

      const newUserGame = await userGameModel.findAll({
        include: ["biodata", "history"],
        where: {
          id: userGameId,
        },
      });

      res.json(newUserGame);
    } else {
      res.json({
        errorMsg: "Maaf Username sudah digunakan mohon pakai username lain",
      });
    }
  } catch (error) {
    res.status(500).json({
      error: error,
    });
  }
});

// delete (user, biodata, history)
router.delete("/:id", async (req, res, next) => {
  const userGameId = parseInt(req.params.id);

  try {
    const findTargetDeleteData = await userGameModel.findOne({
      where: {
        id: userGameId,
      },
    });
    await userGameBiodataModel.destroy({
      where: {
        id: userGameId,
      },
    });
    await userGameHistoryModel.destroy({
      where: {
        id: userGameId,
      },
    });
    await userGameModel.destroy({
      where: {
        id: userGameId,
      },
    });
    res.json({
      msg: `Success delete data with id ${userGameId}`,
      deletedData: {
        username: findTargetDeleteData.username,
        email: findTargetDeleteData.email,
      },
    });
  } catch (error) {
    res.status(500).json({
      error: error,
    });
  }
});

module.exports = router;
