"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_biodata.belongsTo(models.user_game, {
        foreignKey: "user_game_id",
        as: "bidoata",
      });
    }
  }
  user_game_biodata.init(
    {
      user_game_id: DataTypes.INTEGER,
      fullname: DataTypes.STRING,
      age: DataTypes.INTEGER,
      address: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "user_game_biodata",
      timestamps: false,
    }
  );

  // user_game_biodata.associate = function (models) {
  //   user_game_biodata.belongsTo(models.user_game, {
  //     foreignKey: "user_game_id",
  //     as: "user",
  //   });
  // };

  return user_game_biodata;
};
