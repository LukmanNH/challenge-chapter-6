"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_history.belongsTo(models.user_game, {
        foreignKey: "user_game_id",
        as: "history",
      });
    }
  }
  user_game_history.init(
    {
      hoursOfPlayedGame: DataTypes.INTEGER,
      mostPlayedGame: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "user_game_history",
      timestamps: false,
    }
  );
  return user_game_history;
};
