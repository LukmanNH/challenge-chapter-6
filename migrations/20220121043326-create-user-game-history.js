"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("user_game_histories", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      hoursOfPlayedGame: {
        type: Sequelize.INTEGER,
      },
      mostPlayedGame: {
        type: Sequelize.STRING,
      },
      user_game_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "user_games",
          key: "id",
        },
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("user_game_histories");
  },
};
