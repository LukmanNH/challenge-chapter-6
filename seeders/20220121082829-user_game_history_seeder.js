"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("user_game_histories", [
      {
        hoursOfPlayedGame: 3500,
        mostPlayedGame: "Dota",
        user_game_id: 1,
      },
      {
        hoursOfPlayedGame: 300,
        mostPlayedGame: "POU",
        user_game_id: 2,
      },
      {
        hoursOfPlayedGame: 800,
        mostPlayedGame: "PES 2024",
        user_game_id: 3,
      },
      {
        hoursOfPlayedGame: 1500,
        mostPlayedGame: "Dota",
        user_game_id: 4,
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_game_histories", null, {});
  },
};
