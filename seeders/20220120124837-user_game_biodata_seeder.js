"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("user_game_biodata", [
      {
        user_game_id: 1,
        fullname: "Lukman Nur Hakim",
        age: 17,
        address: "Jl. Jakarta",
      },
      {
        user_game_id: 2,
        fullname: "Alif Babrizq Kuncara",
        age: 20,
        address: "Jl. Tulungagung",
      },
      {
        user_game_id: 3,
        fullname: "Aryasatya Okta Pradhana",
        age: 30,
        address: "Jl. Tuban",
      },
      {
        user_game_id: 4,
        fullname: "Galih Putra Windawan",
        age: 25,
        address: "Jl. Batam",
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_game_biodata", null, {});
  },
};
