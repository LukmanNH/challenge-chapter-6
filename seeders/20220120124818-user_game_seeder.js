"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("user_games", [
      {
        username: "lukman",
        password: "anjayMabar",
        email: "lukman@gmail.com",
      },
      {
        username: "alif",
        password: "nais",
        email: "alif@gmail.com",
      },
      {
        username: "arya",
        password: "bakekok",
        email: "arya@gmail.com",
      },
      {
        username: "galih",
        password: "gaylih",
        email: "galih@gmail.com",
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("user_games", null, {});
  },
};
